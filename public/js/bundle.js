/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _css_reset_css__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var _css_main_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6);
/* harmony import */ var _img_logo_png__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(11);
/* harmony import */ var _class_classEL_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(12);
/* harmony import */ var _class_classHeader_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(13);
/* harmony import */ var _class_classFilter_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(25);
/* harmony import */ var _fetch_token__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(20);
/* harmony import */ var _class_classCard__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(21);
/* harmony import */ var _fetch_getCards__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(31);












function createHtml() {
  const body = document.body;
  const container = new _class_classEL_js__WEBPACK_IMPORTED_MODULE_3__["default"]();
  const containerEl = container.createElement('div', ['container']);
  const headerEl = new _class_classHeader_js__WEBPACK_IMPORTED_MODULE_4__["default"]();
  const filterEl = new _class_classFilter_js__WEBPACK_IMPORTED_MODULE_5__["default"]();
  const containerCardEl = container.createElement('div', ['cards-container', 'container'], {}, '<p class="no-items">No items have beed added</p>');
  containerEl.append(headerEl.render(), filterEl.render());
  return body.append(containerEl, containerCardEl);
}

async function createLoginHTML() {
  createHtml();
  document.querySelector('.header__button').classList.add('d-none');
  document.querySelector('.header__button-new').classList.remove('d-none');
  const cards = await Object(_fetch_getCards__WEBPACK_IMPORTED_MODULE_8__["default"])(_fetch_token__WEBPACK_IMPORTED_MODULE_6__["default"]);

  if (cards.length !== 0) {
    document.querySelector('.cards-container').innerHTML = '';
    cards.forEach(el => {
      const card = new _class_classCard__WEBPACK_IMPORTED_MODULE_7__["default"](el);
      card.render();
    });
  }
}

if (localStorage.getItem('email')) {
  createLoginHTML();
} else {
  createHtml();
}

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_reset_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);



var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_reset_css__WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ __webpack_exports__["default"] = (_node_modules_css_loader_dist_cjs_js_reset_css__WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

var stylesInDom = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDom.length; i++) {
    if (stylesInDom[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var index = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3]
    };

    if (index !== -1) {
      stylesInDom[index].references++;
      stylesInDom[index].updater(obj);
    } else {
      stylesInDom.push({
        identifier: identifier,
        updater: addStyle(obj, options),
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function insertStyleElement(options) {
  var style = document.createElement('style');
  var attributes = options.attributes || {};

  if (typeof attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : undefined;

    if (nonce) {
      attributes.nonce = nonce;
    }
  }

  Object.keys(attributes).forEach(function (key) {
    style.setAttribute(key, attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.media ? "@media ".concat(obj.media, " {").concat(obj.css, "}") : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  } else {
    style.removeAttribute('media');
  }

  if (sourceMap && typeof btoa !== 'undefined') {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    if (Object.prototype.toString.call(newList) !== '[object Array]') {
      return;
    }

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDom[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDom[_index].references === 0) {
        stylesInDom[_index].updater();

        stylesInDom.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.i, "html,\nbody,\ndiv,\nspan,\napplet,\nobject,\niframe,\nh1,\nh2,\nh3,\nh4,\nh5,\nh6,\np,\nblockquote,\npre,\na,\nabbr,\nacronym,\naddress,\nbig,\ncite,\ncode,\ndel,\ndfn,\nem,\nimg,\nins,\nkbd,\nq,\ns,\nsamp,\nsmall,\nstrike,\nstrong,\nsub,\nsup,\ntt,\nvar,\nb,\nu,\ni,\ncenter,\ndl,\ndt,\ndd,\nol,\nul,\nli,\nfieldset,\nform,\nlabel,\nlegend,\ntable,\ncaption,\ntbody,\ntfoot,\nthead,\ntr,\nth,\ntd,\narticle,\naside,\ncanvas,\ndetails,\nembed,\nfigure,\nfigcaption,\nfooter,\nheader,\nhgroup,\nmenu,\nnav,\noutput,\nruby,\nsection,\nsummary,\ntime,\nmark,\naudio,\nvideo {\n  margin: 0;\n  padding: 0;\n  border: 0;\n  font-size: 100%;\n  font: inherit;\n  vertical-align: baseline;\n}\n/* HTML5 display-role reset for older browsers */\narticle,\naside,\ndetails,\nfigcaption,\nfigure,\nfooter,\nheader,\nhgroup,\nmenu,\nnav,\nsection {\n  display: block;\n}\nbody {\n  line-height: 1;\n}\nol,\nul {\n  list-style: none;\n}\nblockquote,\nq {\n  quotes: none;\n}\nblockquote:before,\nblockquote:after,\nq:before,\nq:after {\n  content: '';\n  content: none;\n}\ntable {\n  border-collapse: collapse;\n  border-spacing: 0;\n}\n\na {\n  text-decoration: none;\n}\n", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (cssWithMappingToString) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join("");
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === "string") {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, ""]];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_main_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(7);



var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_main_css__WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ __webpack_exports__["default"] = (_node_modules_css_loader_dist_cjs_js_main_css__WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8);
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fonts_PoppinsRegular_woff__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9);
/* harmony import */ var _fonts_Corinthia_Regular_woff__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(10);
// Imports




var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
var ___CSS_LOADER_URL_REPLACEMENT_0___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()(_fonts_PoppinsRegular_woff__WEBPACK_IMPORTED_MODULE_2__["default"]);
var ___CSS_LOADER_URL_REPLACEMENT_1___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()(_fonts_Corinthia_Regular_woff__WEBPACK_IMPORTED_MODULE_3__["default"]);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "@font-face {\n  font-family: 'Poppins';\n  src: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") format('woff');\n}\n@font-face {\n  font-family: 'Corinthia';\n  src: url(" + ___CSS_LOADER_URL_REPLACEMENT_1___ + ") format('woff');\n}\n\nbody {\n  font-size: 20px;\n  font-family: 'Poppins';\n  font-weight: 200;\n}\n.container {\n  width: 100%;\n  margin: 0 auto;\n  padding: 0 40px;\n  box-sizing: border-box;\n  max-width: 1200px;\n}\n\n.page__header {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  padding: 20px 0;\n}\n\n.page__filter {\n  padding: 30px 0;\n  border-bottom: 1px solid gray;\n  border-top: 1px solid gray;\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n}\n\n.button {\n  background-color: #375292;\n  color: white;\n  padding: 8px 15px;\n  font-size: 20px;\n  border-radius: 5px;\n  border: none;\n  cursor: pointer;\n}\n\n.button:hover {\n  background-color: #253966;\n}\n\n.label {\n  align-items: center;\n  margin-top: 10px;\n  margin-bottom: 10px;\n}\n\n.input {\n  width: 200px;\n  font-size: 13px;\n  padding: 6px 10px;\n  border: 1px solid #cecece;\n  background: #f6f6f6;\n  border-radius: 8px;\n  font-size: 20px;\n  margin-left: 10px;\n}\n\n.textarea {\n  overflow: auto;\n  resize: none;\n  width: 200px;\n  height: 80px;\n  background: #f6f6f6;\n  border: 1px solid #cecece;\n  border-radius: 8px;\n  padding: 6px;\n  font-size: 20px;\n}\n\n.select {\n  width: 200px;\n  font-size: 13px;\n  padding: 6px;\n  border: 1px solid #cecece;\n  background: #f6f6f6;\n  border-radius: 8px;\n  font-size: 20px;\n  margin-left: 10px;\n}\n\n.modal {\n  position: fixed;\n  z-index: 1;\n  left: 0;\n  top: 0;\n  width: 100%;\n  height: 100%;\n  overflow: auto;\n  background-color: rgba(0, 0, 0, 0.4);\n}\n\n.modal-content {\n  background-color: #fefefe;\n  top: 50%;\n  transform: translateY(-50%);\n  margin: auto;\n  padding: 40px;\n  width: 35%;\n  text-align: left;\n  position: relative;\n  text-align: left;\n  border-radius: 15px;\n}\n\n.modal-title {\n  font-weight: 700;\n  text-align: center;\n  margin-bottom: 25px;\n}\n\n.d-block {\n  display: block;\n}\n\n.d-none {\n  display: none;\n}\n\n.close-button {\n  color: #aaa;\n  position: absolute;\n  font-size: 28px;\n  font-weight: bold;\n  top: 15px;\n  right: 20px;\n  cursor: pointer;\n  border: none;\n  background-color: transparent;\n}\n\n.done-button {\n  position: absolute;\n  width: 25px;\n  height: 25px;\n  top: 19px;\n  right: 50px;\n  cursor: pointer;\n}\n\n.cards-button {\n  background-color: #aaa;\n  width: 100%;\n  padding: 5px 10px;\n  font-size: 20px;\n  border-radius: 5px;\n  border: none;\n  cursor: pointer;\n  margin: 15px 0 0;\n}\n\n.cards-button:hover {\n  background-color: #375292;\n  color: white;\n}\n\n.cards-container {\n  padding-top: 25px;\n  display: grid;\n  grid-template-columns: repeat(3, 1fr);\n  grid-gap: 25px;\n}\n\n.cards-item {\n  margin-top: 25px;\n  background: #f6f6f6;\n  border: 1px solid #cecece;\n  border-radius: 15px;\n  padding: 20px;\n  position: relative;\n}\n\n.cards-item.done {\n  color: #aaa;\n}\n\n.cards-item-text {\n  margin-bottom: 15px;\n}\n\n.warning-title {\n  color: red;\n  padding-bottom: 10px;\n}\n", ""]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (url, options) {
  if (!options) {
    // eslint-disable-next-line no-param-reassign
    options = {};
  } // eslint-disable-next-line no-underscore-dangle, no-param-reassign


  url = url && url.__esModule ? url.default : url;

  if (typeof url !== "string") {
    return url;
  } // If url is already wrapped in quotes, remove them


  if (/^['"].*['"]$/.test(url)) {
    // eslint-disable-next-line no-param-reassign
    url = url.slice(1, -1);
  }

  if (options.hash) {
    // eslint-disable-next-line no-param-reassign
    url += options.hash;
  } // Should url be wrapped?
  // See https://drafts.csswg.org/css-values-3/#urls


  if (/["'() \t\n]/.test(url) || options.needQuotes) {
    return "\"".concat(url.replace(/"/g, '\\"').replace(/\n/g, "\\n"), "\"");
  }

  return url;
};

/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "fonts/PoppinsRegular.woff");

/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "fonts/Corinthia-Regular.woff");

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/logo.png");

/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
class Element {
  constructor() {
    this.element = null;
  }

  createElement(elemTag, classNames = [], newAtr = {}, text = null) {
    this.element = document.createElement(elemTag);
    this.element.classList.add(...classNames);

    if (text) {
      this.element.innerHTML = text;
    }

    if (newAtr) {
      for (const key in newAtr) {
        this.element.setAttribute(key, newAtr[key]);
      }
    }

    return this.element;
  }

  insertIntoPage(container) {
    container.append(this.element);
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Element);

/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classEL_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(12);
/* harmony import */ var _classButton_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(14);
/* harmony import */ var _classLogin_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(15);
/* harmony import */ var _classVisitCardiologist_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(27);





class ElementHeader extends _classEL_js__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor() {
    super();
  }

  render() {
    this.headerEl = this.createElement('header', ['page__header', 'header']);
    this.headerLogoContainer = this.createElement('a', ['header__logo-link'], {
      href: '#'
    });
    this.headerLogo = this.createElement('img', ['header__logo'], {
      src: 'images/logo.png',
      width: '100px'
    });
    this.headerButtonEl = new _classButton_js__WEBPACK_IMPORTED_MODULE_1__["default"](['header__button', 'button'], 'log-in-button', 'Log in');
    this.headerBtnEl = this.headerButtonEl.create();
    this.buttonClick(this.headerBtnEl);
    this.newHeaderButtonEl = new _classButton_js__WEBPACK_IMPORTED_MODULE_1__["default"](['header__button-new', 'button', 'd-none'], 'create-button', 'Create a visit');
    this.newHeaderBtnEL = this.newHeaderButtonEl.create();
    this.newBtnClick(this.newHeaderBtnEL);
    this.headerLogoContainer.append(this.headerLogo);
    this.headerEl.append(this.headerLogoContainer, this.headerBtnEl, this.newHeaderBtnEL);
    return this.headerEl;
  }

  buttonClick(btnEl) {
    btnEl.addEventListener('click', e => {
      e.preventDefault();
      const loginModal = new _classLogin_js__WEBPACK_IMPORTED_MODULE_2__["default"]();
      loginModal.render();
    });
  }

  newBtnClick(btnEl) {
    btnEl.addEventListener('click', e => {
      e.preventDefault();
      this.visitModalCardiol = new _classVisitCardiologist_js__WEBPACK_IMPORTED_MODULE_3__["default"]();
      this.visitModalCardiol.render();
    });
  }

}

/* harmony default export */ __webpack_exports__["default"] = (ElementHeader);

/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classEL_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(12);


class Button extends _classEL_js__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(classes, id, text) {
    super();
    this.classes = classes;
    this.id = id;
    this.text = text;
  }

  create() {
    this.element = this.createElement("button", this.classes, {
      id: this.id,
      type: "submit"
    }, this.text);
    this.element.addEventListener('click', e => {
      e.preventDefault();
    });
    return this.element;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Button);

/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classInput_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(16);
/* harmony import */ var _classLabel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(17);
/* harmony import */ var _classButton_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(14);
/* harmony import */ var _classModal_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(18);
/* harmony import */ var _fetch_getUserToken_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(19);
/* harmony import */ var _fetch_token_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(20);
/* harmony import */ var _classCard_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(21);
/* harmony import */ var _fetch_getCards_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(31);









class Login {
  constructor() {
    this.closeBtn = this.closeBtn().create();
    this.loginBtn = this.loginBtn().create();
    this.email = this.emailInput().create();
    this.password = this.passwordInput().create();
    this.element = this.createModal().create();
    this.btnHeader = document.querySelector('.header__button');
    this.newBtnHeader = document.querySelector('.header__button-new');
    this.localEmail = localStorage.getItem('email');
    this.localPassword = localStorage.getItem('password');
  }

  render() {
    window.onload = () => {
      this.email.firstElementChild.value = localStorage.getItem('email');
      this.password.firstElementChild.value = localStorage.getItem('password');
    };

    onload();
    document.body.prepend(this.element);
    this.closeBtn.addEventListener('click', this.handleCloseClick.bind(this));
    this.loginBtn.addEventListener('click', this.handleLoginClick.bind(this));
  }

  createModal() {
    return new _classModal_js__WEBPACK_IMPORTED_MODULE_3__["default"]([this.email, this.password, this.loginBtn, this.closeBtn]);
  }

  handleCloseClick() {
    this.email.firstElementChild.value = '';
    this.password.firstElementChild.value = '';
    this.email.firstElementChild.removeAttribute('placeholder');
    this.password.firstElementChild.removeAttribute('placeholder');
    this.element.remove();
  }

  emailInput() {
    const emailInput = new _classInput_js__WEBPACK_IMPORTED_MODULE_0__["default"](['input'], '');
    return new _classLabel_js__WEBPACK_IMPORTED_MODULE_1__["default"](['label', 'd-block'], `Email`, emailInput.create());
  }

  passwordInput() {
    const passwordInput = new _classInput_js__WEBPACK_IMPORTED_MODULE_0__["default"](['input'], '');
    return new _classLabel_js__WEBPACK_IMPORTED_MODULE_1__["default"](['label', 'd-block'], `Password`, passwordInput.create());
  }

  closeBtn() {
    return new _classButton_js__WEBPACK_IMPORTED_MODULE_2__["default"](['close-button'], 'close-button', 'x');
  }

  loginBtn() {
    return new _classButton_js__WEBPACK_IMPORTED_MODULE_2__["default"](['button', 'login-button'], 'login-button', 'Log in');
  }

  async handleLoginClick(btnRemoveEl) {
    const tokenValue = await Object(_fetch_getUserToken_js__WEBPACK_IMPORTED_MODULE_4__["default"])(this.email.firstElementChild.value, this.password.firstElementChild.value);

    if (tokenValue === _fetch_token_js__WEBPACK_IMPORTED_MODULE_5__["default"]) {
      localStorage.setItem('email', this.email.firstElementChild.value);
      localStorage.setItem('password', this.password.firstElementChild.value);
      this.email.firstElementChild.value = '';
      this.password.firstElementChild.value = '';
      this.handleCloseClick();
      this.btnHeader.classList.add('d-none');
      this.newBtnHeader.classList.remove('d-none');
      this.cardEl = await Object(_fetch_getCards_js__WEBPACK_IMPORTED_MODULE_7__["default"])(_fetch_token_js__WEBPACK_IMPORTED_MODULE_5__["default"]);

      if (this.cardEl.length !== 0) {
        document.querySelector(".cards-container").innerHTML = "";
        this.cardEl.forEach(el => {
          const card = new _classCard_js__WEBPACK_IMPORTED_MODULE_6__["default"](el);
          card.render();
        });
      }
    } else {
      this.email.firstElementChild.value = '';
      this.password.firstElementChild.value = '';
      this.email.firstElementChild.setAttribute('placeholder', 'please try again');
      this.password.firstElementChild.setAttribute('placeholder', 'please try again');
    }
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Login);

/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classEL_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(12);


class Input extends _classEL_js__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(classes, id, name) {
    super();
    this.classes = classes;
    this.id = id;
    this.name = name;
  }

  create() {
    this.createElement('input', this.classes, {
      id: this.id,
      type: 'text',
      name: this.name,
      required: ''
    }, '');
    return this.element;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Input);

/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classEL_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(12);


class Label extends _classEL_js__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(classes, text, child) {
    super();
    this.classes = classes;
    this.text = text;
    this.child = child;
  }

  create() {
    this.createElement('label', this.classes, {}, this.text);
    this.element.append(this.child);
    return this.element;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Label);

/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classEL_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(12);


class Modal extends _classEL_js__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(children) {
    super();
    this.children = children;
  }

  create() {
    const container = document.createElement("div");
    container.classList.add("modal");
    this.createElement("form", ["modal-content"]);
    this.children.forEach(child => {
      this.element.append(child);
    });
    container.append(this.element);
    return container;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Modal);

/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const getLogin = async (email, pass) => {
  try {
    const response = await fetch('https://ajax.test-danit.com/api/v2/cards/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: `${email}`,
        password: `${pass}`
      })
    });
    const data = await response.text();
    return data;
  } catch (error) {
    console.error(error);
  }
};

/* harmony default export */ __webpack_exports__["default"] = (getLogin);

/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const token = `78dc3617-0697-46f1-b746-e72c228e21ab`;
/* harmony default export */ __webpack_exports__["default"] = (token);

/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classButton_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(14);
/* harmony import */ var _classEL_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(12);
/* harmony import */ var _classInput_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(16);
/* harmony import */ var _classVisitDentist_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(22);
/* harmony import */ var _classVisitCardiologist_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(27);
/* harmony import */ var _classVisitTherapist_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(30);






const token = `78dc3617-0697-46f1-b746-e72c228e21ab`;

class Card extends _classEL_js__WEBPACK_IMPORTED_MODULE_1__["default"] {
  constructor({
    id,
    doctor,
    target,
    description,
    urgency,
    name,
    pressure,
    BMI,
    deseases,
    age,
    lastVisit
  }) {
    super();
    this.id = id;
    this.doctor = doctor;
    this.target = target;
    this.description = description;
    this.urgency = urgency;
    this.name = name;
    this.pressure = pressure;
    this.BMI = BMI;
    this.deseases = deseases;
    this.age = age;
    this.lastVisit = lastVisit;
    this.showMoreBtn = this.showMoreBtn().create();
    this.editBtn = this.editBtn().create();
    this.doneBtn = this.doneBtn();
    this.deleteBtn = this.deleteBtn().create();
    this.element = null;
  }

  render() {
    this.create();
    this.showMoreBtn.addEventListener('click', this.showMoreListener.bind(this));
    this.editBtn.addEventListener('click', this.editListener.bind(this));
    this.deleteBtn.addEventListener('click', this.deleteListener.bind(this));
    this.doneBtn.addEventListener('click', this.doneBtnListener.bind(this));
  }

  create() {
    this.containerEl = document.querySelector('.cards-container');
    this.element = this.createElement('div', ['cards-item', 'open', this.urgency.toLowerCase()], {
      id: `card-${this.id}`
    });
    this.element.innerHTML = `<p class="cards-item-text cards-item-text-name">Name: ${this.name}</p><p class="cards-item-text">Doctor: ${this.doctor}</p>`;
    this.element.append(this.showMoreBtn, this.editBtn, this.doneBtn, this.deleteBtn);
    return this.containerEl.append(this.element);
  }

  deleteBtn() {
    return new _classButton_js__WEBPACK_IMPORTED_MODULE_0__["default"](['close-button'], 'close-button', 'x');
  }

  doneBtn() {
    return new _classEL_js__WEBPACK_IMPORTED_MODULE_1__["default"]().createElement('input', ['done-button'], {
      type: 'checkbox'
    });
  }

  showMoreBtn() {
    return new _classButton_js__WEBPACK_IMPORTED_MODULE_0__["default"](['button', 'cards-button'], 'show-more-button', 'Show more');
  }

  editBtn() {
    return new _classButton_js__WEBPACK_IMPORTED_MODULE_0__["default"](['button', 'cards-button'], 'edit-button', 'Edit');
  }

  showMoreListener() {
    this.showMoreBtn.remove();
    const urgency = new _classEL_js__WEBPACK_IMPORTED_MODULE_1__["default"]().createElement('p', ['cards-item-text'], {}, `Urgency: ${this.urgency}`);
    const deseases = new _classEL_js__WEBPACK_IMPORTED_MODULE_1__["default"]().createElement('p', ['cards-item-text'], {}, `Deseases: ${this.deseases}`);
    const target = new _classEL_js__WEBPACK_IMPORTED_MODULE_1__["default"]().createElement('p', ['cards-item-text'], {}, `Target: ${this.target}`);
    const description = new _classEL_js__WEBPACK_IMPORTED_MODULE_1__["default"]().createElement('p', ['cards-item-text', 'card-item-description'], {}, `Description: ${this.description}`);
    const pressure = new _classEL_js__WEBPACK_IMPORTED_MODULE_1__["default"]().createElement('p', ['cards-item-text'], {}, `Pressure: ${this.pressure}`);
    const BMI = new _classEL_js__WEBPACK_IMPORTED_MODULE_1__["default"]().createElement('p', ['cards-item-text'], {}, `BMI: ${this.BMI}`);
    const age = new _classEL_js__WEBPACK_IMPORTED_MODULE_1__["default"]().createElement('p', ['cards-item-text'], {}, `Age: ${this.age}`);
    const lastVisit = new _classEL_js__WEBPACK_IMPORTED_MODULE_1__["default"]().createElement('p', ['cards-item-text'], {}, `Last visit: ${this.lastVisit}`);
    this.editBtn.before(urgency, target, description);

    if (this.doctor == 'Cardiologist') {
      this.editBtn.before(pressure, BMI, deseases, age);
    }

    if (this.doctor == 'Dentist') {
      this.editBtn.before(lastVisit);
    }

    if (this.doctor == 'Therapist') {
      this.editBtn.before(age);
    }
  }

  doneBtnListener() {
    this.element.classList.toggle('done');
    this.element.classList.toggle('open');
  }

  editListener() {
    if (this.doctor == 'Cardiologist') {
      const visit = new _classVisitCardiologist_js__WEBPACK_IMPORTED_MODULE_4__["default"]();
      visit.render();
      visit.element.setAttribute('id', this.id);
      visit.name.querySelector('input').value = this.name;
      visit.target.querySelector('input').value = this.target;
      visit.description.querySelector('textarea').value = this.description;
      visit.age.querySelector('input').value = this.age;
      visit.BMI.querySelector('input').value = this.BMI;
      visit.deseases.querySelector('input').value = this.deseases;
      visit.pressure.querySelector('input').value = this.pressure;
      visit.urgency.querySelector('select').querySelector(`option[value=${this.urgency}`).setAttribute('selected', '');
      visit.createBtn.classList.add('update');
      visit.createBtn.innerHTML = 'Update';
    }

    if (this.doctor == 'Dentist') {
      const visit = new _classVisitDentist_js__WEBPACK_IMPORTED_MODULE_3__["default"]();
      visit.render();
      visit.element.setAttribute('id', this.id);
      visit.name.querySelector('input').value = this.name;
      visit.target.querySelector('input').value = this.target;
      visit.description.querySelector('textarea').value = this.description;
      visit.lastVisit.querySelector('input').value = this.lastVisit;
      visit.urgency.querySelector('select').querySelector(`option[value=${this.urgency}`).setAttribute('selected', '');
      visit.createBtn.classList.add('update');
      visit.createBtn.innerHTML = 'Update';
    }

    if (this.doctor == 'Therapist') {
      const visit = new _classVisitTherapist_js__WEBPACK_IMPORTED_MODULE_5__["default"]();
      visit.render();
      visit.element.setAttribute('id', this.id);
      visit.name.querySelector('input').value = this.name;
      visit.target.querySelector('input').value = this.target;
      visit.description.querySelector('textarea').value = this.description;
      visit.age.querySelector('input').value = this.age;
      visit.urgency.querySelector('select').querySelector(`option[value=${this.urgency}`).setAttribute('selected', '');
      visit.createBtn.classList.add('update');
      visit.createBtn.innerHTML = 'Update';
    }
  }

  deleteListener() {
    fetch(`https://ajax.test-danit.com/api/v2/cards/${this.id}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${token}`
      }
    }).then(() => {
      this.element.remove();

      if (!document.querySelector('.cards-item')) {
        document.querySelector('.cards-container').innerHTML = '<p class="no-items">No items have beed added</p>';
      }
    });
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Card);

/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classInput__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(16);
/* harmony import */ var _classLabel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(17);
/* harmony import */ var _classModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(18);
/* harmony import */ var _classVisit__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(23);
/* harmony import */ var _classCard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(21);
/* harmony import */ var _listener_visitListener_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(26);
/* harmony import */ var _fetch_postVisitFetch_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(28);
/* harmony import */ var _fetch_putVisitCard_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(29);
/* harmony import */ var _fetch_token_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(20);










class VisitDentist extends _classVisit__WEBPACK_IMPORTED_MODULE_3__["default"] {
  constructor() {
    super();
    this.lastVisit = this.lastVisitInput().create();
    this.element = this.createModal().create();
  }

  createModal() {
    const modal = new _classModal__WEBPACK_IMPORTED_MODULE_2__["default"]([this.doctor, this.name, this.urgency, this.target, this.description, this.lastVisit, this.warning, this.createBtn, this.closeBtn]);
    this.doctor.querySelector("select").querySelector(`option[value='Dentist'`).setAttribute("selected", "");
    this.doctor.addEventListener("change", e => {
      Object(_listener_visitListener_js__WEBPACK_IMPORTED_MODULE_5__["default"])(e);
    });
    return modal;
  }

  lastVisitInput() {
    const input = new _classInput__WEBPACK_IMPORTED_MODULE_0__["default"](["input"], "");
    return new _classLabel__WEBPACK_IMPORTED_MODULE_1__["default"](["label", "d-block"], `Last visit date`, input.create());
  }

  handleCreateClick() {
    if (this.createBtn.classList.contains("update")) {
      this.upgradeCard();
    } else {
      this.createCard();
    }
  }

  setPostObj() {
    return {
      name: `${this.name.querySelector("input").value}`,
      description: `${this.description.querySelector("textarea").value}`,
      doctor: `${this.doctor.querySelector("select").value}`,
      urgency: `${this.urgency.querySelector("select").value}`,
      lastVisit: `${this.lastVisit.querySelector("input").value}`,
      target: `${this.target.querySelector("input").value}`
    };
  }

  async createCard() {
    if (this.lastVisit.querySelector("input").value !== "" && this.target.querySelector("input").value !== "" && this.name.querySelector("input").value !== "") {
      if (document.querySelector(".no-items")) {
        document.querySelector(".no-items").remove();
      }

      this.warning.classList.add("d-none");
      const response = await Object(_fetch_postVisitFetch_js__WEBPACK_IMPORTED_MODULE_6__["default"])(this.setPostObj(), _fetch_token_js__WEBPACK_IMPORTED_MODULE_8__["default"]);
      const card = new _classCard__WEBPACK_IMPORTED_MODULE_4__["default"](await response);
      card.render();
      this.element.remove();
    } else {
      this.warning.classList.remove("d-none");
    }
  }

  async upgradeCard() {
    const response = await Object(_fetch_putVisitCard_js__WEBPACK_IMPORTED_MODULE_7__["default"])(this.element.id, this.setPostObj(), _fetch_token_js__WEBPACK_IMPORTED_MODULE_8__["default"]);
    document.getElementById(`card-${this.element.id}`).remove();
    const card = new _classCard__WEBPACK_IMPORTED_MODULE_4__["default"](await response);
    card.render();
    this.element.remove();
  }

}

/* harmony default export */ __webpack_exports__["default"] = (VisitDentist);

/***/ }),
/* 23 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classInput_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(16);
/* harmony import */ var _classLabel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(17);
/* harmony import */ var _classSelect_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(24);
/* harmony import */ var _classButton_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(14);
/* harmony import */ var _classModal_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(18);
/* harmony import */ var _classEL_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(12);
/* harmony import */ var _classFilter_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(25);








class Visit extends _classFilter_js__WEBPACK_IMPORTED_MODULE_6__["default"] {
  constructor() {
    super();
    this.doctor = this.labelDoctor();
    this.createBtn = this.createButton().create();
    this.closeBtn = this.closeBtn().create();
    this.target = this.targetInput().create();
    this.description = this.descriptionInput().create();
    this.urgency = this.selectUrgency();
    this.name = this.nameInput().create();
    this.warning = this.warningTitle();
    this.element = this.createModal().create();
  }

  render() {
    document.addEventListener('click', e => {
      if (e.target.classList.contains('modal')) {
        this.element.remove();
      }
    });
    document.body.prepend(this.element);
    this.closeBtn.addEventListener('click', this.handleCloseClick.bind(this));

    if (this.handleCreateClick) {
      this.createBtn.addEventListener('click', this.handleCreateClick.bind(this));
    }
  }

  createModal() {
    return new _classModal_js__WEBPACK_IMPORTED_MODULE_4__["default"]([this.doctor, this.name, this.urgency, this.target, this.description, this.warning, this.createBtn, this.closeBtn]);
  }

  labelDoctor() {
    this.labelDoctor = this.renderLabelEl(['label', 'd-block'], 'Select doctor');
    this.labelDoctor.append(this.selectDoctor());
    return this.labelDoctor;
  }

  warningTitle() {
    const warning = new _classEL_js__WEBPACK_IMPORTED_MODULE_5__["default"]();
    return warning.createElement('p', ['warning-title', 'd-none'], {}, 'All fields have to be filled');
  }

  selectDoctor() {
    this.selectDoctor = this.renderLabelChildren('select-doctor');
    this.selectDoctor.append(this.renderOptionEl('Cardiologist'));
    this.selectDoctor.append(this.renderOptionEl('Dentist'));
    this.selectDoctor.append(this.renderOptionEl('Therapist'));
    return this.selectDoctor;
  }

  selectDoctorListener() {
    this.selectDoctorEl = this.selectDoctor();
    this.selectDoctorEl.addEventListener('change', e => {
      console.log(e.target.value);
    });
  }

  nameInput() {
    const nameInput = new _classInput_js__WEBPACK_IMPORTED_MODULE_0__["default"](['input'], '', 'name');
    return new _classLabel_js__WEBPACK_IMPORTED_MODULE_1__["default"](['label', 'd-block'], `Name`, nameInput.create());
  }

  selectUrgency() {
    this.labelUrgency = this.renderLabelEl(['label', 'd-block'], 'Select urgency');
    this.selectUrgency = this.renderLabelChildren('');
    this.selectUrgency.append(this.renderOptionEl('High'));
    this.selectUrgency.append(this.renderOptionEl('Normal'));
    this.selectUrgency.append(this.renderOptionEl('Low'));
    this.labelUrgency.append(this.selectUrgency);
    return this.labelUrgency;
  }

  targetInput() {
    const targetInput = new _classInput_js__WEBPACK_IMPORTED_MODULE_0__["default"](['input'], '', 'target');
    return new _classLabel_js__WEBPACK_IMPORTED_MODULE_1__["default"](['label', 'd-block'], `Target`, targetInput.create());
  }

  descriptionInput() {
    const descriptionInput = new _classEL_js__WEBPACK_IMPORTED_MODULE_5__["default"]();
    return new _classLabel_js__WEBPACK_IMPORTED_MODULE_1__["default"](['label', 'd-block'], `Descrition`, descriptionInput.createElement('textarea', ['textarea'], {
      cols: '30',
      rows: '10',
      required: ''
    }, ''));
  }

  createButton() {
    return new _classButton_js__WEBPACK_IMPORTED_MODULE_3__["default"](['button'], 'create-button', 'Create');
  }

  closeBtn() {
    return new _classButton_js__WEBPACK_IMPORTED_MODULE_3__["default"](['close-button'], 'close-button', 'x');
  }

  handleCloseClick() {
    this.element.remove();
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Visit);

/***/ }),
/* 24 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classEL_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(12);


class Select extends _classEL_js__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor(id, text) {
    super();
    this.id = id;
    this.text = text;
  }

  create() {
    this.createElement("select", ["select"], {
      id: this.id
    }, this.text);
    return this.element;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Select);

/***/ }),
/* 25 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classEL_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(12);
/* harmony import */ var _classLabel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(17);
/* harmony import */ var _classInput_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(16);
/* harmony import */ var _classSelect_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(24);





class FilterEl extends _classEL_js__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor() {
    super();
  }

  renderLabelEl(classLabel, textLabel) {
    this.labelFilterEl = new _classLabel_js__WEBPACK_IMPORTED_MODULE_1__["default"](classLabel, textLabel, '');
    return this.labelFilterEl.create();
  }

  renderLabelChildren(idElSelect) {
    this.selectFilterEL = new _classSelect_js__WEBPACK_IMPORTED_MODULE_3__["default"](idElSelect);
    return this.selectFilterEL.create();
  }

  renderOptionEl(text) {
    return this.optionEl = this.createElement('option', ['option'], {
      value: text
    }, text);
  }

  render() {
    this.containerEl = this.createElement('div', ['page__filter']);
    this.label1 = this.renderLabelEl(['label'], 'Text');
    this.inputFilterEl1 = new _classInput_js__WEBPACK_IMPORTED_MODULE_2__["default"](['input'], 'filterInput');
    this.inputEl = this.inputFilterEl1.create();
    this.inputEl.addEventListener('input', () => {
      this.cardEl = document.querySelectorAll('.cards-item');
      Array.from(this.cardEl).forEach(el => {
        const nameEl = el.querySelector('.cards-item-text-name');
        const name = nameEl.textContent.slice(6);

        if (!name.toLowerCase().includes(this.inputEl.value.toLowerCase())) {
          el.classList.add('d-none');
        } else {
          el.classList.remove('d-none');
        }
      });
    });
    this.label1.append(this.inputEl);
    this.label2 = this.renderLabelEl(['label'], 'Status');
    this.select1 = this.renderLabelChildren('selectFilter');
    this.select1.append(this.renderOptionEl('Open'));
    this.select1.append(this.renderOptionEl('Done'));
    this.select1.addEventListener('change', e => {
      this.cardEl = document.querySelectorAll('.cards-item');
      this.selectValue = e.target.value.toLowerCase();
      Array.from(this.cardEl).forEach(el => {
        if (el.classList.contains(this.selectValue)) {
          el.classList.remove('d-none');
        } else {
          el.classList.add('d-none');
        }
      });
    });
    this.label2.append(this.select1);
    this.label3 = this.renderLabelEl(['label'], 'Urgency');
    this.select2 = this.renderLabelChildren('selectFilter');
    this.select2.append(this.renderOptionEl('High'));
    this.select2.append(this.renderOptionEl('Normal'));
    this.select2.append(this.renderOptionEl('Low'));
    this.select2.addEventListener('change', e => {
      this.cardEl = document.querySelectorAll('.cards-item');
      Array.from(this.cardEl).forEach(el => {
        if (!el.classList.contains(e.target.value.toLowerCase())) {
          el.classList.add('d-none');
        } else {
          el.classList.remove('d-none');
        }
      });
    });
    this.label3.append(this.select2);
    this.containerEl.append(this.label1, this.label2, this.label3);
    return this.containerEl;
  }

}

/* harmony default export */ __webpack_exports__["default"] = (FilterEl);

/***/ }),
/* 26 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _class_classVisitCardiologist__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(27);
/* harmony import */ var _class_classVisitDentist__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(22);
/* harmony import */ var _class_classVisitTherapist__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(30);




const selectDoctorListener = e => {
  const modalEl = document.querySelector('.modal');

  if (e.target.value === 'Cardiologist') {
    modalEl.remove();
    const modalCardiologist = new _class_classVisitCardiologist__WEBPACK_IMPORTED_MODULE_0__["default"]();
    modalCardiologist.render();
  } else if (e.target.value == 'Dentist') {
    modalEl.remove();
    const modalDentist = new _class_classVisitDentist__WEBPACK_IMPORTED_MODULE_1__["default"]();
    modalDentist.render();
  } else if (e.target.value == 'Therapist') {
    modalEl.remove();
    const modalTherapist = new _class_classVisitTherapist__WEBPACK_IMPORTED_MODULE_2__["default"]();
    modalTherapist.render();
  }
};

/* harmony default export */ __webpack_exports__["default"] = (selectDoctorListener);

/***/ }),
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classVisit_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(23);
/* harmony import */ var _classInput_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(16);
/* harmony import */ var _classLabel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(17);
/* harmony import */ var _classModal_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(18);
/* harmony import */ var _classCard_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(21);
/* harmony import */ var _listener_visitListener_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(26);
/* harmony import */ var _fetch_postVisitFetch_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(28);
/* harmony import */ var _fetch_putVisitCard_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(29);
/* harmony import */ var _fetch_token_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(20);










class VisitCardiologist extends _classVisit_js__WEBPACK_IMPORTED_MODULE_0__["default"] {
  constructor() {
    super();
    this.pressure = this.pressureInput().create();
    this.BMI = this.BMI().create();
    this.deseases = this.deseasesInput().create();
    this.age = this.ageInput().create();
    this.element = this.createModal().create();
  }

  createModal() {
    const modal = new _classModal_js__WEBPACK_IMPORTED_MODULE_3__["default"]([this.doctor, this.name, this.urgency, this.target, this.description, this.pressure, this.BMI, this.deseases, this.age, this.warning, this.createBtn, this.closeBtn]);
    this.doctor.querySelector('select').querySelector(`option[value='Cardiologist'`).setAttribute('selected', '');
    this.doctor.addEventListener('change', e => {
      Object(_listener_visitListener_js__WEBPACK_IMPORTED_MODULE_5__["default"])(e);
    });
    return modal;
  }

  pressureInput() {
    const input = new _classInput_js__WEBPACK_IMPORTED_MODULE_1__["default"](['input'], '');
    return new _classLabel_js__WEBPACK_IMPORTED_MODULE_2__["default"](['label', 'd-block'], `Pressure`, input.create());
  }

  BMI() {
    const input = new _classInput_js__WEBPACK_IMPORTED_MODULE_1__["default"](['input'], '');
    return new _classLabel_js__WEBPACK_IMPORTED_MODULE_2__["default"](['label', 'd-block'], `BMI`, input.create());
  }

  deseasesInput() {
    const input = new _classInput_js__WEBPACK_IMPORTED_MODULE_1__["default"](['input'], '');
    return new _classLabel_js__WEBPACK_IMPORTED_MODULE_2__["default"](['label', 'd-block'], `Deseases`, input.create());
  }

  ageInput() {
    const input = new _classInput_js__WEBPACK_IMPORTED_MODULE_1__["default"](['input'], '');
    return new _classLabel_js__WEBPACK_IMPORTED_MODULE_2__["default"](['label', 'd-block'], `Age`, input.create());
  }

  handleCreateClick() {
    if (this.createBtn.classList.contains('update')) {
      this.upgradeCard();
    } else {
      this.createCard();
    }
  }

  setPostObj() {
    return {
      name: `${this.name.querySelector('input').value}`,
      description: `${this.description.querySelector('textarea').value}`,
      doctor: `${this.doctor.querySelector('select').value}`,
      urgency: `${this.urgency.querySelector('select').value}`,
      pressure: `${this.pressure.querySelector('input').value}`,
      age: `${this.age.querySelector('input').value}`,
      BMI: `${this.BMI.querySelector('input').value}`,
      deseases: `${this.deseases.querySelector('input').value}`,
      target: `${this.target.querySelector('input').value}`
    };
  }

  async createCard() {
    if (this.name.querySelector('input').value !== '' && this.pressure.querySelector('input').value !== '' && this.BMI.querySelector('input').value !== '' && this.deseases.querySelector('input').value !== '' && this.age.querySelector('input').value !== '' && this.target.querySelector('input').value !== '') {
      if (document.querySelector('.no-items')) {
        document.querySelector('.no-items').remove();
      }

      this.warning.classList.add('d-none');
      const response = await Object(_fetch_postVisitFetch_js__WEBPACK_IMPORTED_MODULE_6__["default"])(this.setPostObj(), _fetch_token_js__WEBPACK_IMPORTED_MODULE_8__["default"]);
      const card = new _classCard_js__WEBPACK_IMPORTED_MODULE_4__["default"](await response);
      card.render();
      this.element.remove();
    } else {
      this.warning.classList.remove('d-none');
    }
  }

  async upgradeCard() {
    const response = await Object(_fetch_putVisitCard_js__WEBPACK_IMPORTED_MODULE_7__["default"])(this.element.id, this.setPostObj(), _fetch_token_js__WEBPACK_IMPORTED_MODULE_8__["default"]);
    document.getElementById(`card-${this.element.id}`).remove();
    const card = new _classCard_js__WEBPACK_IMPORTED_MODULE_4__["default"](await response);
    card.render();
    this.element.remove();
  }

}

/* harmony default export */ __webpack_exports__["default"] = (VisitCardiologist);

/***/ }),
/* 28 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const postVisitFetch = async (bodyObj, token) => {
  try {
    const response = await fetch('https://ajax.test-danit.com/api/v2/cards', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(bodyObj)
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
};

/* harmony default export */ __webpack_exports__["default"] = (postVisitFetch);

/***/ }),
/* 29 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const putVisitCard = async (element, objEl, token) => {
  try {
    const response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${element}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`
      },
      body: JSON.stringify(objEl)
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
};

/* harmony default export */ __webpack_exports__["default"] = (putVisitCard);

/***/ }),
/* 30 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classInput__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(16);
/* harmony import */ var _classLabel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(17);
/* harmony import */ var _classModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(18);
/* harmony import */ var _classVisit__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(23);
/* harmony import */ var _classCard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(21);
/* harmony import */ var _listener_visitListener_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(26);
/* harmony import */ var _fetch_postVisitFetch_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(28);
/* harmony import */ var _fetch_putVisitCard_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(29);
/* harmony import */ var _fetch_token_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(20);










class VisitTherapist extends _classVisit__WEBPACK_IMPORTED_MODULE_3__["default"] {
  constructor() {
    super();
    this.age = this.ageInput().create();
    this.element = this.createModal().create();
  }

  createModal() {
    const modal = new _classModal__WEBPACK_IMPORTED_MODULE_2__["default"]([this.doctor, this.name, this.urgency, this.target, this.description, this.age, this.warning, this.createBtn, this.closeBtn]);
    this.doctor.querySelector('select').querySelector(`option[value='Therapist'`).setAttribute('selected', '');
    this.doctor.addEventListener('change', e => {
      Object(_listener_visitListener_js__WEBPACK_IMPORTED_MODULE_5__["default"])(e);
    });
    return modal;
  }

  ageInput() {
    const input = new _classInput__WEBPACK_IMPORTED_MODULE_0__["default"](['input'], '');
    return new _classLabel__WEBPACK_IMPORTED_MODULE_1__["default"](['label', 'd-block'], `Age`, input.create());
  }

  handleCreateClick() {
    if (this.createBtn.classList.contains('update')) {
      this.upgradeCard();
    } else {
      this.createCard();
    }
  }

  setPostObj() {
    return {
      name: `${this.name.querySelector('input').value}`,
      description: `${this.description.querySelector('textarea').value}`,
      doctor: `${this.doctor.querySelector('select').value}`,
      urgency: `${this.urgency.querySelector('select').value}`,
      target: `${this.target.querySelector('input').value}`,
      age: `${this.age.querySelector('input').value}`
    };
  }

  async createCard() {
    if (this.target.querySelector('input').value !== '' && this.name.querySelector('input').value !== '' && this.age.querySelector('input').value !== '') {
      if (document.querySelector('.no-items')) {
        document.querySelector('.no-items').remove();
      }

      this.warning.classList.add("d-none");
      const response = await Object(_fetch_postVisitFetch_js__WEBPACK_IMPORTED_MODULE_6__["default"])(this.setPostObj(), _fetch_token_js__WEBPACK_IMPORTED_MODULE_8__["default"]);
      const card = new _classCard__WEBPACK_IMPORTED_MODULE_4__["default"](await response);
      card.render();
      this.element.remove();
    } else {
      this.warning.classList.remove("d-none");
    }
  }

  async upgradeCard() {
    const response = await Object(_fetch_putVisitCard_js__WEBPACK_IMPORTED_MODULE_7__["default"])(this.element.id, this.setPostObj(), _fetch_token_js__WEBPACK_IMPORTED_MODULE_8__["default"]);
    document.getElementById(`card-${this.element.id}`).remove();
    const card = new _classCard__WEBPACK_IMPORTED_MODULE_4__["default"](await response);
    card.render();
    this.element.remove();
  }

}

/* harmony default export */ __webpack_exports__["default"] = (VisitTherapist);

/***/ }),
/* 31 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const getCards = async token => {
  try {
    const response = await fetch('https://ajax.test-danit.com/api/v2/cards', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
};

/* harmony default export */ __webpack_exports__["default"] = (getCards);

/***/ })
/******/ ]);